// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Weapon/WeaponDefault.h"
#include "Character/TPSInventoryComponent.h"
#include "Character/TPSCharacterHealthComponent.h"
#include "Interface/TPS_IGameActor.h"
#include "StateEffects/TPS_StateEffect.h"
#include "TopDownShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

	//Input Flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Effect Section
	UPROPERTY(Replicated)
	TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//Cursor Section
	UDecalComponent* CurrentCursor = nullptr;

	//Inputs Function Begin
	void InputAxisX(float value);
	void InputAxisY(float value);

	void InputActionWalk();
	void InputActionSprint();
	void InputActionAim();

	void InputAttackPressed();
	void InputAttackReleased();

	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();
	//Inputs Function End

	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}
	//Input Function End
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;
	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	/** A shield that protects the player from damage */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* ShieldMesh;

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* ComponentInventory;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* HealthComponentChar;

	//Cursor Section
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);


	//Movement Section
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float  AngleForSprint	= 35.0f;
	//Movement Section
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bSprintRunEbabled	= false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalkedEnabled		= false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bAimEnabled		= false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAngleNormal		= false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsShiftPressed	= false;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	
	//Health and Alive Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alive")
		TArray<UAnimMontage*> DeadsAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alive")
		USoundBase* DeadSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;

	//Weapon Section
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;
	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(Replicated)
		int32 CurrentIndexWeapon = 0;

	//Timer 
	FTimerHandle DecreaseStaminaHandle;
	FTimerHandle IncreaseStaminaHandle;
	FTimerHandle TimerHandle_RagDollTimer;
	
	//Stamina Section
	UPROPERTY(BlueprintReadOnly)
		float StaminaCurrent;
	UPROPERTY(BlueprintReadOnly)
		float StaminaMin = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float StaminaMax				 = 30.0f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float IntensityDecreaseStamina	 = 0.2f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float IntensityIncreaseStamina	 = 0.1f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float DelayBeforeIncreaseStamina = 3.0f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float StaminaIncreaseValue		 = 0.1f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float StaminaDecreaseValue		 = 0.5f;
	UPROPERTY(EditDefaultsOnly, Category = "StaminaSystem")
		float StaminaRestart			 = 2.5f;

	//Tick Function()
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Movement and Stamina Section begin
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		void UpdateAngleSpeedRun();
	UFUNCTION(BlueprintCallable)
		void StartSprint();
	UFUNCTION(BlueprintCallable)
		void StopSprint();
	UFUNCTION(BlueprintCallable)
		void ChangeStamina();
	//Movement and Stamina Section End

	//Weapon Section begin
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName idWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void DropCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	//Weapon Section End

	//Interface begin
	EPhysicalSurface GetSurfuceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
		void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* NewEffect);
		void AddEffect_Implementation(UTPS_StateEffect* NewEffect) override;
	//Interface end
	
	//Get And Set Function Begin
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTPS_StateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();
	//Get And Set Function End

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();
	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	//Online Function Begin
	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewMovementState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewMovementState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_Multicast(UAnimMontage* Animation);

	UFUNCTION()
	void EffectAdd_OnRep();

	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);

	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);
};