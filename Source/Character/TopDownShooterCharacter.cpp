// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Game/TPSGameInstance.h"
#include "Weapon/Projectile/ProjectileDefault.h"
#include "TopDownShooter.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"



//Default Section
ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw	= false;
	bUseControllerRotationRoll	= false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement	= true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate				= FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane			= true;
	GetCharacterMovement()->bSnapToPlaneAtStart			= true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength	 = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a ShieldMesh
	ShieldMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShieldMesh"));
	ShieldMesh->SetupAttachment(GetMesh());
	ShieldMesh->SetWorldScale3D(FVector(2.0f, 2.0f, 2.0f));
	ShieldMesh->SetVisibility(false);
	ShieldMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);


	//Stamina Section
	StaminaCurrent = StaminaMax;

	//Initialization InventoryComponent
	ComponentInventory	= CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponentChar = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponentChar)
	{
		HealthComponentChar->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharDead);
	}	
	if (ComponentInventory)
	{
		ComponentInventory->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Network
	bReplicates = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//Cursor Section
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_Authority || GetLocalRole() == ROLE_AutonomousProxy))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}


}



//Input Section Begin
void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	//Axis Move Forward-Backward and Right-Left
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);

	//Action 'LeftShift' for SprintRun and Call Timer of Stamina
	NewInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATopDownShooterCharacter::InputActionSprint);
	NewInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATopDownShooterCharacter::InputActionSprint);
	
	//Action 'LeftAlt' for Walk
	NewInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &ATopDownShooterCharacter::InputActionWalk);
	NewInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &ATopDownShooterCharacter::InputActionWalk);

	//Action 'RightMouse' for Aim
	NewInputComponent->BindAction(TEXT("AimState"), IE_Pressed, this, &ATopDownShooterCharacter::InputActionAim);
	NewInputComponent->BindAction(TEXT("AimState"), IE_Released, this, &ATopDownShooterCharacter::InputActionAim);

	//Action 'LeftMouse' for Attack
	NewInputComponent->BindAction(TEXT("FireEvent"),   EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"),   EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::TryReloadWeapon);

	//Action 'NextWeapon' for SwitchWeapon
	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchPreviosWeapon);

	//Action 'Q' for Ability Effect
	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TryAbilityEnabled);

	//Action 'F' for Ability Effect
	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<0>);
}

void ATopDownShooterCharacter::InputAxisX(float value)
{
	AxisX = value;
}
void ATopDownShooterCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATopDownShooterCharacter::InputActionWalk()
{
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (PlayerController->IsInputKeyDown(EKeys::LeftAlt))
		{
			bWalkedEnabled = true;
			ChangeMovementState();
		}
		else
		{
			bWalkedEnabled = false;
			ChangeMovementState();
		}
	}
}
void ATopDownShooterCharacter::InputActionSprint()
{
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (PlayerController->IsInputKeyDown((EKeys::LeftShift)))
		{
			bIsShiftPressed = true;
			if (bIsAngleNormal)
			{
				bSprintRunEbabled = true;
				ChangeMovementState();
				StartSprint();
			}
		}
		else
		{
			if (bSprintRunEbabled)
			{
				bSprintRunEbabled = false;
				StopSprint();
				ChangeMovementState();
			}
			bIsShiftPressed = false;
		}
	}
}
void ATopDownShooterCharacter::InputActionAim()
{
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (PlayerController->IsInputKeyDown(EKeys::RightMouseButton))
		{
			bAimEnabled = true;
			ChangeMovementState();
		}
		else
		{
			bAimEnabled = false;
			ChangeMovementState();
		}
	}
}

void ATopDownShooterCharacter::InputAttackPressed()
{
	if (HealthComponentChar && HealthComponentChar->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}
void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}
//Input Section End



//Movement Section Begin
void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	if (HealthComponentChar && HealthComponentChar->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{

			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTPS_Network, Warning, TEXT("Movement State - %s"), *SEnum);

			if (MovementState == EMovementState::SprintRun_State)
			{
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				
				SetActorRotation(FQuat(myRotator));
				SetActorRotationByYaw_OnServer(myRotator.Yaw);

				UpdateAngleSpeedRun();
			}
			else
			{
				APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (MyController)
				{
					FHitResult ResultHit;
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
					
					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					
					SetActorRotation(FQuat(FRotator(0.f, FindRotatorResultYaw, 0.f)));
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

					UpdateAngleSpeedRun();

					if (CurrentWeapon)
					{
						FVector Displacment = FVector(0);
						bool bIsReduceDispertion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacment = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispercion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::AimWalk_State:
							Displacment = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispercion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::Walk_State:
							Displacment = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispercion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::Run_State:
							Displacment = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispercion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;
						}

						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacment;

						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacment, bIsReduceDispertion);
					}
				}
			}

		}
	}
}

void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	if (GetCurrentWeapon())
	{
		//ToDo Check melee or range
		GetCurrentWeapon()->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}

}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState()
{
	EMovementState NewMovementState = EMovementState::Run_State;
	if (!bWalkedEnabled && !bSprintRunEbabled && !bAimEnabled)
	{
		NewMovementState = EMovementState::Run_State;
	}
	else
	{
		if (bSprintRunEbabled)
		{
			bWalkedEnabled = false;
			bAimEnabled = false;
			NewMovementState = EMovementState::SprintRun_State;
		}
		else
		{
			if (bWalkedEnabled && !bSprintRunEbabled && bAimEnabled)
			{
				NewMovementState = EMovementState::AimWalk_State;
			}
			else
			{
				if (bWalkedEnabled && !bSprintRunEbabled && !bAimEnabled)
				{
					NewMovementState = EMovementState::Walk_State;
				}
				else
				{
					if (!bWalkedEnabled && !bSprintRunEbabled && bAimEnabled)
					{
						NewMovementState = EMovementState::Aim_State;
					}
				}
			}
		}
	}
	
	SetMovementState_OnServer(NewMovementState);
	

	//Weapon State Update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewMovementState);
	}
}

void ATopDownShooterCharacter::UpdateAngleSpeedRun()
{
	
	FVector Velocity = GetVelocity();
	FVector ForwardVector = GetActorForwardVector(); 
	Velocity.Normalize();
	
	float Angle = FMath::Acos(FVector::DotProduct(Velocity, ForwardVector)) * (180.0f / PI);
	if (Angle <= AngleForSprint)
	{
		bIsAngleNormal = true;
	}
	else
	{
		if (bSprintRunEbabled)
		{
			bSprintRunEbabled = false;
			ChangeMovementState();
		}
		bIsAngleNormal = false;
	}
}

void ATopDownShooterCharacter::StartSprint()
{
	if (bIsAngleNormal && bIsShiftPressed) // If Rules True We can sprint
	{
		//Turn ON Sprint for change Movement Speed
		bSprintRunEbabled = true;
		//Run Timer for decrease Stamina Value
		GetWorld()->GetTimerManager().SetTimer(DecreaseStaminaHandle, this, &ATopDownShooterCharacter::ChangeStamina, IntensityDecreaseStamina, true);
	}
	//Run Change Movement Select Logic after compare prev. logic
	ChangeMovementState();
}

void ATopDownShooterCharacter::StopSprint()
{
	if (GetWorld()->GetTimerManager().IsTimerActive(DecreaseStaminaHandle))
	{
		GetWorld()->GetTimerManager().ClearTimer(DecreaseStaminaHandle);
	}
	if (GetWorld()->GetTimerManager().IsTimerActive(IncreaseStaminaHandle))
	{
		GetWorld()->GetTimerManager().ClearTimer(IncreaseStaminaHandle);
	}
	bSprintRunEbabled = false;
	GetWorld()->GetTimerManager().SetTimer(IncreaseStaminaHandle, this, &ATopDownShooterCharacter::ChangeStamina, IntensityIncreaseStamina, true, DelayBeforeIncreaseStamina);
	ChangeMovementState();
}

void ATopDownShooterCharacter::ChangeStamina()
{
	if (bSprintRunEbabled)
	{
		if (bIsAngleNormal && bIsShiftPressed)
		{
			if (StaminaCurrent - StaminaDecreaseValue > 0.0f)
			{
				StaminaCurrent -= StaminaDecreaseValue;
			}
			else
			{
				StaminaCurrent = 0.0f;
				StopSprint();
			}
		}
		else
		{
			StopSprint();
		}
	}
	else
	{
		if (StaminaCurrent < StaminaMax)
		{
			StaminaCurrent += StaminaIncreaseValue;
		}
		else
		{
			StaminaCurrent = StaminaMax;
			if (GetWorld()->GetTimerManager().IsTimerActive(IncreaseStaminaHandle))
			{
				GetWorld()->GetTimerManager().ClearTimer(IncreaseStaminaHandle);
			}
		}
	}
}
//Movement Section End



//Inventory Section Begin
void ATopDownShooterCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && ComponentInventory->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AddicionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (ComponentInventory)
		{
			if (ComponentInventory->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
				//to dl all
			}
		}
	}
}
void ATopDownShooterCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && ComponentInventory->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AddicionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (ComponentInventory)
		{
			if (ComponentInventory->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
				//to dl all
			}
		}
	}
}
void ATopDownShooterCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && ComponentInventory->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && ComponentInventory)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAddicionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AddicionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}

				bIsSuccess = ComponentInventory->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
			}
		}
	}
}
//Inventory Section End



//Weapon Section Begin
void ATopDownShooterCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					if (IdWeaponName == "GrenadeGun")
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHandGrenadeGun"));
					}
					else if (IdWeaponName == "SniperGun")
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHandSniperGun"));
					}
					else if (IdWeaponName == "RocketGun")
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHandRocketGun"));
					}
					else
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					}
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->AddicionalWeaponInfo = WeaponAddicionalInfo;
					
					
					CurrentIndexWeapon = NewCurrentIndexWeapon;
					

					//Not Forgot remove delegate on change/drop weapon
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}
					
					if (ComponentInventory)
					{
						ComponentInventory->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}
void ATopDownShooterCharacter::DropCurrentWeapon()
{
	if (ComponentInventory)
	{
		ComponentInventory->DropWeapobByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATopDownShooterCharacter::TryReloadWeapon()
{
	if (HealthComponentChar && HealthComponentChar->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
}
void ATopDownShooterCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}
void ATopDownShooterCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (CurrentWeapon)
	{
		Anim = CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFire;
	}

	if (ComponentInventory && CurrentWeapon)
	{
		ComponentInventory->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AddicionalWeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}
void ATopDownShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (ComponentInventory && CurrentWeapon)
	{
		ComponentInventory->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		ComponentInventory->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AddicionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATopDownShooterCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}
void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}
void ATopDownShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}
//Weapon Section End



//Interface Section Begin
EPhysicalSurface ATopDownShooterCharacter::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponentChar)
	{
		if (HealthComponentChar->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}
TArray<UTPS_StateEffect*> ATopDownShooterCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}
void ATopDownShooterCharacter::RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}


}
void ATopDownShooterCharacter::AddEffect_Implementation(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}
//Interface Section End



//Get And Set Section Begin
AWeaponDefault* ATopDownShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}
int32 ATopDownShooterCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}
bool ATopDownShooterCharacter::GetIsAlive()
{
	bool bIsResult = false;
	if (HealthComponentChar)
	{
		bIsResult = HealthComponentChar->GetIsAlive();
	}
	return bIsResult;
}
UDecalComponent* ATopDownShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}
EMovementState ATopDownShooterCharacter::GetMovementState()
{
	return MovementState;
}
TArray<UTPS_StateEffect*> ATopDownShooterCharacter::GetAllCurrentEffects()
{
	return Effects;
}
//Get And Set Section End



//Private Section
void ATopDownShooterCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			PlayAnim_Multicast(DeadsAnim[rnd]);

			UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeadSound, GetActorLocation());
			ShieldMesh->SetVisibility(false);
		}


		if (GetController())
		{
			GetController()->UnPossess();
		}

		//Timer
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATopDownShooterCharacter::EnableRagdoll_Multicast, TimeAnim, false);

		SetLifeSpan(3.f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(3.f);
		}
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}
		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
}

void ATopDownShooterCharacter::CharDead_BP_Implementation()
{
	//In BP
}

void ATopDownShooterCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn,ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActoalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (HealthComponentChar && HealthComponentChar->GetIsAlive())
	{
		HealthComponentChar->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}
	return ActoalDamage;
}

void ATopDownShooterCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}
//Private Section End



//Online Function Begin
void ATopDownShooterCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATopDownShooterCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.f, Yaw, 0.f)));
	}
}

void ATopDownShooterCharacter::SetMovementState_OnServer_Implementation(EMovementState NewMovementState)
{
	SetMovementState_Multicast(NewMovementState);
}

void ATopDownShooterCharacter::SetMovementState_Multicast_Implementation(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATopDownShooterCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void ATopDownShooterCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Animation)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Animation);
	}
}

void ATopDownShooterCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{ 
		SwitchEffect(EffectAdd, true);
	}
}

void ATopDownShooterCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATopDownShooterCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATopDownShooterCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}


void ATopDownShooterCharacter::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		while (i < ParticleSystemEffects.Num() && !bIsFind)
		{
			if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && ParticleSystemEffects[i]->Template == Effect->ParticleEffect)
			{
				bIsFind = true;
				ParticleSystemEffects[i]->DeactivateSystem();
				ParticleSystemEffects[i]->DestroyComponent();
				ParticleSystemEffects.RemoveAt(i);
			}	
			i++;
		}
	}
}

bool ATopDownShooterCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i <Effects.Num();i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATopDownShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATopDownShooterCharacter, MovementState);
	DOREPLIFETIME(ATopDownShooterCharacter, CurrentWeapon);
	DOREPLIFETIME(ATopDownShooterCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATopDownShooterCharacter, Effects);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectAdd);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectRemove);
}


