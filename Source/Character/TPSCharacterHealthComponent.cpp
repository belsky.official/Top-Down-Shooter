// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	if (bIsNoEffectSuper)
	{
		Shield += ChangeValue;

		ShieldChangeEvent_Multicast(Shield, ChangeValue);

		if (Shield > 100.0f)
		{
			Shield = 100.0f;
		}
		else
		{
			if (Shield < 0.0f)
			{
				Shield = 0.0f;
			}
		}

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UTPSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoverRateTimer);
		}
	}
}

void UTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoverRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoverRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float ChangeValue)
{
	OnShieldChange.Broadcast(NewShield, ChangeValue);
}
		