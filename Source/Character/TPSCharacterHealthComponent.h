// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TOPDOWNSHOOTER_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()
	
protected:

	float Shield = 100.0f;

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CoolDownShieldTimer;	
	FTimerHandle TimerHandle_ShieldRecoverRateTimer;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	bool bIsNoEffectSuper = true;

	//Function

	void ChangeHealthValue_OnServer(float ChangeValue) override;
	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
	float GetShieldValue();

	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float NewShield, float ChangeValue);
};

		