// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectile/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	

protected:
	//Caled when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//Celled every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bIsDebugOn		 = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
		bool  TimerEnabled   = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
		float TimeToExplose  = 5.0f;
	float TimerToExplose = 0.0f;

	//function
	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	UFUNCTION(Server, Reliable)
	void Explose_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeFX_Multicast(UParticleSystem* FxTemplate);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeSound_Multicast(USoundBase* ExplodeSound);
};
