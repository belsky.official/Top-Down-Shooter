// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TopDownShooterPlayerController.generated.h"

UCLASS()
class ATopDownShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownShooterPlayerController();

protected:

	virtual void OnUnPossess() override;
};