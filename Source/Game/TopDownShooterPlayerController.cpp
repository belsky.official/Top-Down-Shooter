// Copyright Epic Games, Inc. All Rights Reserved.

#include "Game/TopDownShooterPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Character/TopDownShooterCharacter.h"
#include "Engine/World.h"

ATopDownShooterPlayerController::ATopDownShooterPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATopDownShooterPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}