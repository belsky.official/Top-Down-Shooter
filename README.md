# TopDownShooter Multiplayer

Developed with Unreal Engine 4 by TheBelskx

Обучающий проект Top-Down-Shooter Multiplayer, разработанный на Unreal Engine 4, послужил основой для изучения механик, использованных позднее в MobWars Online. Аналогичная по стилю игре Crimsonland 2003 года, игра представляет собой шутер с видом сверху, где игрок уничтожает постоянно меняющихся врагов в каждом раунде, завершая игру битвой с боссом.